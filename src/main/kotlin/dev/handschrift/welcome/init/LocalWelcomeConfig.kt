package dev.handschrift.welcome.init

data class LocalWelcomeConfig(val welcomeChannelId: String,
                              val forceJoinAnnouncement: Boolean,
                              val welcomeMessage: String,
                              val joinRoles: List<String>)
