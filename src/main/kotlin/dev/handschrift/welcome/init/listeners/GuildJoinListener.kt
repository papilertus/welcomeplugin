package dev.handschrift.welcome.init.listeners

import dev.handschrift.welcome.init.LocalWelcomeConfig
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class GuildJoinListener(val config: LocalWelcomeConfig): ListenerAdapter() {
    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {
        val user = event.user
        for(roleId in config.joinRoles){
            val role = event.guild.getRoleById(roleId)
            if(role != null)
                event.guild.addRoleToMember(user, role).queue()

            if(!config.forceJoinAnnouncement){
                user.openPrivateChannel().queue {
                   
                }
            }

        }


    }
}