package dev.handschrift.welcome.init

import com.papilertus.command.Command
import com.papilertus.gui.contextMenu.ContextMenuEntry
import com.papilertus.plugin.Plugin
import com.papilertus.plugin.PluginData
import net.dv8tion.jda.api.hooks.EventListener

class WelcomePlugin: Plugin {
    override fun getCommands(): List<Command> {
        return listOf()
    }

    override fun getContextMenuEntries(): List<ContextMenuEntry> {
        return listOf()
    }

    override fun getListeners(): List<EventListener> {
        return listOf()
    }

    override fun onLoad(data: PluginData) {

    }

    override fun onUnload() {

    }
}